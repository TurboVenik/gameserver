#Для запуска bower от имени администратора
echo '{ "allow_root": true }' > /root/.bowerrc

#Запуск проекта в фоновом режиме prod - на виртуальном сервере, dev - на локалке
sudo nohup mvn spring-boot:run -D spring.profiles.active=prod &

#Необходимые программы
sudo apt-get install nodejs
sudo ln -s /usr/bin/nodejs /usr/bin/node
sudo apt-get install npm
npm i -g bower