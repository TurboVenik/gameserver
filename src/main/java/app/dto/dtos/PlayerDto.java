package app.dto.dtos;

public class PlayerDto {

    private String id;

    private float x;
    private float y;
    private String direction;

    public PlayerDto(String id, float x, float y, String direction) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.direction = direction;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }
}
