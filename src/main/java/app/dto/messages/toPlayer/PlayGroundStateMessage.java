package app.dto.messages.toPlayer;

import app.dto.messages.Message;
import app.dto.messages.fromPlayer.PlayerBoltsMessage;
import app.dto.dtos.PlayerDto;

import java.util.List;

public class PlayGroundStateMessage extends Message {

    {
        setType("playground_state_message");
    }
    private List<PlayerDto> playerDtos;

    private List<PlayerBoltsMessage> bolts;


    public List<PlayerDto> getPlayerDtos() {
        return playerDtos;
    }

    public void setPlayerDtos(List<PlayerDto> playerDtos) {
        this.playerDtos = playerDtos;
    }

    public List<PlayerBoltsMessage> getBolts() {
        return bolts;
    }

    public void setBolts(List<PlayerBoltsMessage> bolts) {
        this.bolts = bolts;
    }
}
