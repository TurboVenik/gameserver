package app.dto.messages.toPlayer;

import app.dto.messages.Message;

public class DeathMessage extends Message {

    {
        setType("death_message");
    }

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
