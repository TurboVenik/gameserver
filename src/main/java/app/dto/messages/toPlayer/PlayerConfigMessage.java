package app.dto.messages.toPlayer;

import app.dto.messages.Message;

public class PlayerConfigMessage extends Message {

    {
        setType("player_config_message");
    }

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
