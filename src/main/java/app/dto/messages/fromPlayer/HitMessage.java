package app.dto.messages.fromPlayer;

import app.dto.messages.Message;

public class HitMessage extends Message{

    {
        setType("hit_message");
    }

    private String dealDmg;
    private String takeDmg;
    private int dmg;

    public String getDealDmg() {
        return dealDmg;
    }

    public void setDealDmg(String dealDmg) {
        this.dealDmg = dealDmg;
    }

    public String getTakeDmg() {
        return takeDmg;
    }

    public void setTakeDmg(String takeDmg) {
        this.takeDmg = takeDmg;
    }

    public int getDmg() {
        return dmg;
    }

    public void setDmg(int dmg) {
        this.dmg = dmg;
    }
}
