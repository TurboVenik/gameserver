package app.dto.messages.fromPlayer;

import app.dto.dtos.CoordsDto;
import app.dto.messages.Message;

import java.util.ArrayList;
import java.util.List;

public class PlayerBoltsMessage extends Message {
    {
        setType("player_bolts_message");
    }
    private String id;

    private List<CoordsDto> coordinates = new ArrayList<CoordsDto>();

    public List<CoordsDto> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(List<CoordsDto> coordinates) {
        this.coordinates = coordinates;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
