package app.dto.messages.fromPlayer;

import app.dto.messages.Message;

public class ResurrectPlayerMessage extends Message {

    {
       setType("resurrect_player");
    }

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
