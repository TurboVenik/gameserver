package app.dto.messages.fromPlayer;

import app.dto.messages.Message;

public class PlayerStateMessage extends Message{

    {
        setType("player_coordinates_message");
    }

    private float x;
    private float y;

    private String direction;

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }
}
