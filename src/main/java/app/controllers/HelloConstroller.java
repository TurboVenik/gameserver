package app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HelloConstroller {

    @Autowired
    private ServerProperties serverProperties;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView hello() {
        ModelAndView mav = new ModelAndView();

        mav.setViewName("game");

        mav.addObject("serverHostName", serverProperties.getAddress().getHostAddress());
        mav.addObject("serverPort", serverProperties.getPort());

        return mav;
    }
}
