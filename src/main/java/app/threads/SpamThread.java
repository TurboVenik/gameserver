package app.threads;

import app.dto.dtos.CoordsDto;
import app.dto.messages.fromPlayer.PlayerBoltsMessage;
import app.dto.dtos.PlayerDto;
import app.dto.messages.fromPlayer.ResurrectPlayerMessage;
import app.dto.messages.toPlayer.PlayGroundStateMessage;
import app.gamesession.PlayerSession;
import com.google.gson.Gson;
import org.springframework.web.socket.TextMessage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Date;

public class SpamThread extends Thread {

    private boolean works;
    private Gson gson = new Gson();

    public Map<String, PlayerSession> sessions = new ConcurrentHashMap<String, PlayerSession>();

    @Override
    public void run() {
        while (true) {
            if (sessions.size() == 0) {
                try {
                    Thread.sleep(100);
                    continue;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            PlayGroundStateMessage messageDto = new PlayGroundStateMessage();
            List<PlayerDto> playerDtoList = new ArrayList<PlayerDto>();
            List<CoordsDto> bolts = new ArrayList<CoordsDto>();
            messageDto.setBolts(new ArrayList<PlayerBoltsMessage>());
            for (PlayerSession o: sessions.values()) {
                if (o.isDead()) {
                    continue;
                }
                PlayerDto dto = new PlayerDto(o.getSession().getId(), o.getX(), o.getY(), o.getDirection());
                playerDtoList.add(dto);
                bolts.addAll(o.getBoltsCoords());
                PlayerBoltsMessage playerBoltsDto = new PlayerBoltsMessage();
                playerBoltsDto.setId(o.getSession().getId());
                playerBoltsDto.setCoordinates(o.getBoltsCoords());
                messageDto.getBolts().add(playerBoltsDto);
            }

            messageDto.setPlayerDtos(playerDtoList);

            TextMessage message = new TextMessage(gson.toJson(messageDto));
            for (PlayerSession session : sessions.values()) {
                try {
                    session.getSession().sendMessage(message);
                } catch (Exception e) {
                    //e.printStackTrace();
                }
            }

            for (PlayerSession session : sessions.values()) {
                if (session.isWantToRes()) {
                    if (new Date().getTime() - session.getDeathTime().getTime() > session.getDeathDuration()) {
                        session.setWantToRes(false);
                        session.setDead(false);
                        session.setHealth(5);
                        ResurrectPlayerMessage resurrectPlayerMessage = new ResurrectPlayerMessage();
                        resurrectPlayerMessage.setId(session.getSession().getId());

                        message = new TextMessage(gson.toJson(resurrectPlayerMessage));
                        try {
                            session.getSession().sendMessage(message);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            try {
                Thread.sleep(16);
            } catch (InterruptedException e) {
                //e.printStackTrace();
            }
        }
    }

    public boolean isWorks() {
        return works;
    }
}
