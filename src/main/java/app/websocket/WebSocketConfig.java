package app.websocket;

import app.dto.messages.Message;
import app.dto.messages.fromPlayer.HitMessage;
import app.dto.messages.fromPlayer.PlayerBoltsMessage;
import app.dto.messages.fromPlayer.PlayerStateMessage;
import app.dto.messages.fromPlayer.ResurrectPlayerMessage;
import app.dto.messages.toPlayer.PlayerConfigMessage;
import app.gamesession.GameSession;
import app.gamesession.PlayerSession;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;

@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {

    @Autowired
    GameSession gameSession;

    Gson gson = new Gson();

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(new MyMessageHandler(), "/game");
    }

    class MyMessageHandler extends TextWebSocketHandler {

        //private List<WebSocketSession> sessions = new CopyOnWriteArrayList<WebSocketSession>();

        @Override
        public void afterConnectionEstablished(WebSocketSession session) {
            System.out.println("session");
            PlayerSession playerSession = new PlayerSession();
            playerSession.setSession(session);
            playerSession.setX(-100);
            playerSession.setY(-100);
            gameSession.addNewPlayer(playerSession);

            PlayerConfigMessage configMessage = new PlayerConfigMessage();
            configMessage.setId(session.getId());
            String message = gson.toJson(configMessage);
            try {
                session.sendMessage(new TextMessage(message));
            } catch (IOException e) {
                e.printStackTrace();
            }

            //gameSession.startSpam();
            //.add(session);
        }

        @Override
        protected  void handleTextMessage(WebSocketSession session, TextMessage message) {
            Message dto = gson.fromJson(message.getPayload(), Message.class);

            if (dto.getType().equals("player_coordinates_message")) {
                PlayerStateMessage playerStateMessage = gson.fromJson(message.getPayload(), PlayerStateMessage.class);
                gameSession.updatePlayer(session.getId(), playerStateMessage);
            } else if (dto.getType().equals("bolts_coordinates_message")) {
                PlayerBoltsMessage playerBoltsMessage = gson.fromJson(message.getPayload(), PlayerBoltsMessage.class);
                gameSession.updateBolts(session.getId(), playerBoltsMessage);
            } else if (dto.getType().equals("hit_message")) {
                HitMessage hitMessage = gson.fromJson(message.getPayload(), HitMessage.class);
                gameSession.hitPlayer(session.getId(), hitMessage);
            } else if (dto.getType().equals("resurrect_player")) {
                ResurrectPlayerMessage resurrectPlayer = gson.fromJson(message.getPayload(), ResurrectPlayerMessage.class);
                gameSession.resurrectPlayer(session.getId(), resurrectPlayer);
            }

            //gameSession.updatePlayer(session.getId(), dto);
            //System.out.println(messagetoplayer);
            /*for (WebSocketSession s : sessions) {
                try {
                    s.sendMessage(messagetoplayer);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }*/
        }

        @Override
        public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
            gameSession.removePlayer(session.getId());
        }
    }

}
