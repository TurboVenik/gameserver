package app.gamesession;

import app.dto.messages.fromPlayer.HitMessage;
import app.dto.messages.fromPlayer.PlayerBoltsMessage;
import app.dto.messages.fromPlayer.PlayerStateMessage;
import app.dto.messages.fromPlayer.ResurrectPlayerMessage;
import app.dto.messages.toPlayer.DeathMessage;
import app.threads.SpamThread;
import com.google.gson.Gson;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class GameSession {
    private Gson gson = new Gson();

    private SpamThread thread;
    private boolean started;

    private Map<String, PlayerSession> sessions = new ConcurrentHashMap<String, PlayerSession>();

    @PostConstruct
    private void init() {
        thread = new SpamThread();
        thread.sessions = sessions;
        thread.start();
    }

    public void addNewPlayer(PlayerSession session) {
        sessions.put(session.getSession().getId(), session);
    }

    public void updatePlayer(String id, PlayerStateMessage dto) {
        PlayerSession playerSession = sessions.get(id);

        playerSession.setX(dto.getX());
        playerSession.setY(dto.getY());
        playerSession.setDirection(dto.getDirection());
    }


    public void updateBolts(String id, PlayerBoltsMessage dto) {
        PlayerSession playerSession = sessions.get(id);

        playerSession.setBoltsCoords(dto.getCoordinates());
    }

    public void removePlayer(String id) {
        sessions.remove(id);
    }

    public void startSpam() {
        thread.start();
    }

    public Map<String, PlayerSession> getSessions() {
        return sessions;
    }

    public boolean isStarted() {
        return started;
    }

    public void setStarted(boolean started) {
        this.started = started;
    }

    public void hitPlayer(String id, HitMessage hitMessage) {
        PlayerSession playerSession = sessions.get(hitMessage.getTakeDmg());
        playerSession.setHealth(playerSession.getHealth() - hitMessage.getDmg());

        if (playerSession.getHealth() <= 0) {
            playerSession.setDead(true);

            playerSession.setDeathTime(new Date());

            DeathMessage deathMessage = new DeathMessage();
            deathMessage.setId(hitMessage.getTakeDmg());
            TextMessage message = new TextMessage(gson.toJson(deathMessage));

            for (PlayerSession session : sessions.values()) {
                try {
                    synchronized (session) {
                        session.getSession().sendMessage(message);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void resurrectPlayer(String id, ResurrectPlayerMessage resurrectPlayer) {
        PlayerSession playerSession = sessions.get(resurrectPlayer.getId());
        playerSession.setWantToRes(true);
    }
}
