package app.gamesession;

import app.dto.dtos.CoordsDto;
import org.springframework.web.socket.WebSocketSession;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PlayerSession {

    private WebSocketSession session;

    private float x;
    private float y;
    private String direction;

    private int health = 5;

    private boolean isDead = false;
    private Date deathTime;
    private int deathDuration = 4_000;
    private boolean wantToRes = false;

    private List<CoordsDto> boltsCoords = new ArrayList< CoordsDto>();

    public WebSocketSession getSession() {
        return session;
    }

    public void setSession(WebSocketSession session) {
        this.session = session;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public List<CoordsDto> getBoltsCoords() {
        return boltsCoords;
    }

    public void setBoltsCoords(List<CoordsDto> boltsCoords) {
        this.boltsCoords = boltsCoords;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public boolean isDead() {
        return isDead;
    }

    public void setDead(boolean dead) {
        isDead = dead;
    }

    public Date getDeathTime() {
        return deathTime;
    }

    public void setDeathTime(Date deathTime) {
        this.deathTime = deathTime;
    }

    public int getDeathDuration() {
        return deathDuration;
    }

    public void setDeathDuration(int deathDuration) {
        this.deathDuration = deathDuration;
    }

    public boolean isWantToRes() {
        return wantToRes;
    }

    public void setWantToRes(boolean wantToRes) {
        this.wantToRes = wantToRes;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }
}
