class Player {

    constructor(game) {
        this.secondJump = false;
        this.secondJumpF = true;
        this.game = game;
        this.playerObj = game.add.sprite(32, game.world.height - 150, 'dude');

        game.physics.arcade.enable(this.playerObj);
        //player.body.bounce.y = 0.2;
        this.playerObj.body.gravity.y = 1800;
        this.playerObj.body.collideWorldBounds = true;

        this.playerObj.animations.add('left', [0, 1, 2, 3], 10, true);
        this.playerObj.animations.add('right', [5, 6, 7, 8], 10, true);

        game.camera.follow(this.playerObj);

        this.alive = true;

        this.bolts = game.add.group();

        this.bolts.enableBody = true;


        this.reloadTime = 125;
        this.lastFireTime = 0;
        this.reloaded = true;

        this.id = -1;
        //var style = { font: "bold 32px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" };
        //var style = { font: "bold 16px Arial", boundsAlignV: "middle"};
        //this.text = game.add.text(this.playerObj.x,this.playerObj.y, "", style);
    }

    playerDead() {
        this.alive = false;
        this.playerObj.loadTexture('dead_dude', 0);
        this.playerObj.animations.add('left', [0, 1, 2, 3], 10, true);
        this.playerObj.animations.add('right', [5, 6, 7, 8], 10, true);
    }

    playerAlive() {
        this.alive = true;
        this.playerObj.loadTexture('dude', 0);
        this.playerObj.animations.add('left', [0, 1, 2, 3], 10, true);
        this.playerObj.animations.add('right', [5, 6, 7, 8], 10, true);
    }

    playerMove() {
        this.playerObj.body.velocity.x = 0;

        if (this.game.input.keyboard.isDown(Phaser.Keyboard.A)) {
            this.playerObj.body.velocity.x = -200;
            this.playerObj.animations.play('left');
            this.playerObj.direction = 'left';
        } else if (this.game.input.keyboard.isDown(Phaser.Keyboard.D)) {
            this.playerObj.body.velocity.x = 200;
            this.playerObj.animations.play('right');
            this.playerObj.direction = 'right';
        } else {
            this.playerObj.animations.stop();
            this.playerObj.frame = 4;
            this.playerObj.direction = 'stay';
        }
        if (!this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)
            && !this.playerObj.body.touching.down
            && this.playerObj.secondJumpF){
            this.playerObj.secondJump = true;
            this.playerObj.secondJumpF = false;
        }
        if (this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)){
            if (this.playerObj.body.touching.down) {
                this.playerObj.body.velocity.y = -550;
                this.playerObj.secondJumpF = true;
                this.playerObj.secondJump = false;
            } else if (this.playerObj.secondJump) {
                this.playerObj.body.velocity.y = -550;
                this.playerObj.secondJump = false;
            }
        }
        if (this.text) {
            this.text.x = this.playerObj.x  + 16;
            this.text.y = this.playerObj.y;
        }
    }

    playerFire() {
        if (this.game.input.mousePointer.isDown && (Date.now() - this.lastFireTime) > this.reloadTime) {
            this.lastFireTime = Date.now();
            var bolt = this.bolts.create(this.playerObj.body.x, this.playerObj.body.y, 'bolt');
            this.game.physics.arcade.moveToPointer(bolt, 500);
        }
    }
}


