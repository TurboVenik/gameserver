var playGroundWith = 1200;//+ 400;
var playGroundHeigth = 700;//+ 300;

var weigth = 1920;
var heigth = 900;
/*
win_w = $(window).width();
win_h = $(window).height();
*/
var aa = [];

var enemys;
var enemyBolts;

var player;
var webSocket;
var platforms;
var cursors;

var stars;
var score = 0;
var scoreText;
var c = window.weigth;
var b = 900 / 1920 * c;
var game;
var host;
var port;
//console.log(123);
$("input").click(function () {
    console.log(123);
});

function start(serverHost, serverPort) {
    host = serverHost;
    port = serverPort;
    game =
        new Phaser.Game(1920 * 0.7, 900 * 0.7, Phaser.ARCADE, 'play_ground', {
            preload: preload,
            create: create,
            update: update,
            init: init
        });

}

function init() {
    game.stage.disableVisibilityChange = true;
}

function preload() {
    game.load.image('sky', 'assets/png/sky.png');
    game.load.image('ground', 'assets/png/platform.png');
    game.load.image('star', 'assets/png/star.png');
    game.load.image('bolt', 'assets/png/bolt.png');
    game.load.spritesheet('dude', 'assets/png/dude.png', 32, 48);
    game.load.spritesheet('dead_dude', 'assets/png/dead_dude.png', 32, 48);
}

function create() {
    createMap();
    createPlayer();
    createStars();
    createEnemy();
    cursors = game.input.keyboard;
    game.input.mouse.capture = true;
    $("canvas")[0].id = "play_room";
    //$("canvas").removeAttr("height");


}

function update() {
    //game.world.setBounds(0, 0, 10, 10);
    collisions();
    player.playerMove();

    if (player.alive) {
        player.playerFire();
        webSocket.send(JSON.stringify(
            {
                type: "player_coordinates_message",
                x: player.playerObj.body.x,
                y: player.playerObj.body.y,
                direction: player.playerObj.direction
            })
        );
    } else if (game.input.keyboard.isDown(Phaser.Keyboard.R)) {

        webSocket.send(JSON.stringify(
            {
                type: "resurrect_player",
                id: player.id
            })
        );
    }


}

function killSecond(first, second) {
    second.kill();
}

function killSecond1(first, second) {
    player.bolts.remove(second);
    second.kill();
}

function collectStars(first, second) {
    second.destroy();
    score += 10;
    $("#score").html("Score: " + score);
}

function killBoth(first, second) {
    first.kill();
    second.kill();
}

function playerHited(first, second) {
    //c.push(second);
    //console.log("hitted");
}

function enemyHitted(first, second) {
    c.push(first);
    console.log(player.id + "hit " + second.id);
    webSocket.send(JSON.stringify(
        {
            type: "hit_message",
            dealDmg: player.id,
            takeDmg: second.id,
            dmg: 1
        })
    );
}

function killBoth1(first, second) {
    //bolts.remove(first);
    first.kill();
    second.kill();
}

var c = [];

function killBolt(first, second) {
    c.push(first);
}

function createPlayer() {
    player = new Player(game);
    webSocket = new Socket(player, host, port);

    enemyBolts = game.add.group();

    enemyBolts.enableBody = true;


}

function createMap() {
    game.physics.startSystem(Phaser.Physics.ARCADE);
    game.world.setBounds(0, 0, weigth, heigth);
    game.add.tileSprite(0, 0, 1920, 1920, 'sky');

    platforms = game.add.group();
    platforms.enableBody = true;

    var ground = platforms.create(0, game.world.height - 30, 'ground');
    ground.scale.setTo(20, 3);
    ground.body.immovable = true;

    var ledge = platforms.create(300, game.world.height - 200, 'ground'); // 1
    ledge.body.immovable = true;

    ledge = platforms.create(700, game.world.height - 120, 'ground'); // 2
    //ledge.scale.setTo(1, 10);
    ledge.body.immovable = true;

    ledge = platforms.create(500, game.world.height - 350, 'ground'); // 3
    ledge.scale.setTo(0.1, 8);
    ledge.body.immovable = true;

    ledge = platforms.create(50, game.world.height - 150, 'ground'); // 4
    ledge.scale.setTo(0.5, 1);
    ledge.body.immovable = true;

    ledge = platforms.create(150, game.world.height - 500, 'ground'); // 5
    ledge.scale.setTo(0.8, 1);
    ledge.body.immovable = true;

    ledge = platforms.create(-150, game.world.height - 350, 'ground'); // 6
    ledge.body.immovable = true;

    ledge = platforms.create(700, game.world.height - 560, 'ground'); // 7
    ledge.scale.setTo(0.8, 1);
    ledge.body.immovable = true;

    ledge = platforms.create(900, game.world.height - 710, 'ground'); // 8
    ledge.scale.setTo(0.1, 8);
    ledge.body.immovable = true;

    ledge = platforms.create(900, game.world.height - 390, 'ground'); // 9
    ledge.scale.setTo(0.1, 9);
    ledge.body.immovable = true;

    ledge = platforms.create(850, game.world.height - 260, 'ground'); // 10
    ledge.scale.setTo(0.4, 1);
    ledge.body.immovable = true;
    ledge = platforms.create(1200, game.world.height - 320, 'ground'); // 11
    ledge.scale.setTo(0.9, 1);
    ledge.body.immovable = true;
    ledge = platforms.create(1179, game.world.height - 710, 'ground'); // 12
    ledge.scale.setTo(0.1, 8);
    ledge.body.immovable = true;
    ledge = platforms.create(1200, game.world.height - 710, 'ground'); // 13
    ledge.scale.setTo(1.2, 1);
    ledge.body.immovable = true;
    ledge = platforms.create(1600, game.world.height - 710, 'ground'); // 14
    ledge.scale.setTo(0.1, 14);
    ledge.body.immovable = true;
    ledge = platforms.create(1350, game.world.height - 420, 'ground'); // 15
    ledge.scale.setTo(0.8, 1);
    ledge.body.immovable = true;
    ledge = platforms.create(1200, game.world.height - 550, 'ground'); // 16
    ledge.scale.setTo(0.8, 1);
    ledge.body.immovable = true;
    ledge = platforms.create(1300, game.world.height - 170, 'ground'); // 17
    //ledge.scale.setTo(1, 10);
    ledge.body.immovable = true;
    ledge = platforms.create(1800, game.world.height - 280, 'ground'); // 18
    ledge.scale.setTo(0.1, 5);
    ledge.body.immovable = true;
    ledge = platforms.create(1620, game.world.height - 560, 'ground'); // 19
    ledge.scale.setTo(0.15, 1);
    ledge.body.immovable = true;
}

function createStars() {
    stars = game.add.group();
    stars.enableBody = true;
    for (var i = 0; i < 12; i++) {
        var star = stars.create(i * 70, 0, 'star');
        star.body.gravity.y = 300;
        star.body.bounce.y = 0.7 + Math.random() * 0.2;
    }
}

function createEnemy() {
    enemys = game.add.group();
    //game.physics.arcade.enable(enemys);
}

function collisions() {
    game.physics.arcade.collide(player.playerObj, platforms);
    //game.physics.arcade.collide(enemys, platforms);
    game.physics.arcade.collide(stars, platforms);

    game.physics.arcade.overlap(player.playerObj, stars, collectStars, null, this);

    game.physics.arcade.overlap(player.bolts, platforms, killBolt, null, this);


    let bb = [];
    player.bolts.forEach(function (item) {
        if (item.x > weigth || item.x < 0 || item.y < 0 || item.y > heigth) {
            aa.push(item);

        } else {

            bb.push({x: item.x, y: item.y});
        }

    });

    webSocket.send(JSON.stringify(
        {
            type: "bolts_coordinates_message",
            coordinates: bb
        })
    );

    for (let i = 0; i < aa.length; i++) {
        player.bolts.remove(aa[i]);
    }
    aa = [];


    game.physics.arcade.overlap(player.playerObj, enemyBolts, playerHited, null, this);


    enemys.forEach(function (enemy) {
        player.bolts.forEach(function (bolt) {

            if (Phaser.Rectangle.intersects(bolt.getBounds(), enemy.getBounds())) {
                enemyHitted(bolt, enemy);
            }
        });
    });
    //game.physics.arcade.overlap(player.bolts, enemys, enemyHitted, null, this);

    for (let i = 0; i < c.length; i++)
        c[i].destroy();
    /*game.physics.arcade.overlap(player, stars, collectStars, null, this);
    game.physics.arcade.overlap(player, enemys, killSecond, null, this);


    */

}

