var style = { font: "bold 16px Arial", boundsAlignV: "middle"};

class Socket {

    constructor(player, host, port) {
        this.socket = new WebSocket('ws://' + host + ':' + port + '/game');
        this.player = player;
        this.socket.onmessage = function(event) {
            var message = JSON.parse(event.data);

            if (message.type == "player_config_message") {

                //player.text = game.add.bitmapText(player.playerObj.x,player.playerObj.y, "bold 16px Arial", "123321123123name_" + message.id, 64);
                //player.text = new BitmapText(game )

                player.text = game.add.text(player.playerObj.x + 16, player.playerObj.y, "name_" + message.id, style);
                player.text.anchor.x = 0.5;
                player.text.anchor.y = 0.5;
                player.id = message.id;

            } else if (message.type == "playground_state_message" && enemys != null) {
                enemys.forEach(function(item){
                    item.visited = false;
                });
                for (let i = 0; i < message.playerDtos.length; i++) {
                    if (message.playerDtos[i].id != player.id) {
                        var f = false;
                        enemys.forEach(function(item) {

                            if (item.id == message.playerDtos[i].id) {
                                item.visited = true;
                                if(message.playerDtos[i].direction == 'right') {
                                    item.animations.play('right');
                                } else if(message.playerDtos[i].direction == 'left') {
                                    item.animations.play('left');
                                } else if(message.playerDtos[i].direction == 'stay'){
                                    item.animations.stop();
                                    item.frame = 4;
                                }

                                item.x = message.playerDtos[i].x;
                                item.y = message.playerDtos[i].y;

                                item.text.x = item.x  + 16;
                                item.text.y = item.y;

                                //item.position = new Point(message.playerDtos[i].x,message.playerDtos[i].y);
                                f = true;
                            }
                        });
                        if (!f) {
                            console.log("new");
                            var enemy = game.add.sprite(message.playerDtos[i].x, message.playerDtos[i].y, 'dude')
                            enemys.add(enemy);
                            enemy.visited = true;
                            enemy.animations.add('left', [0, 1, 2, 3], 10, true);
                            enemy.animations.add('right', [5, 6, 7, 8], 10, true);
                            enemy.animations.play('left');
                            enemy.id = message.playerDtos[i].id;

                            enemy.text = game.add.text(enemy.x + 16, enemy.y, "name_" + enemy.id, style);
                            enemy.text.anchor.x = 0.5;
                            enemy.text.anchor.y = 0.5;
                        }
                    }
                }
                var des = []
                enemys.forEach(function(item){
                    if (!item.visited) {
                        des.push (item);
                        item.text.destroy();
                    }
                });

                for (let i = 0; i < des.length; i++) {

                    enemys.remove(des[i]);
                    //des[i].destroy();
                }

                enemyBolts.removeAll();
                for (let i = 0; i < message.bolts.length; i++) {
                    if (message.bolts[i].id != player.id) {
                         for(let j = 0; j < message.bolts[i].coordinates.length; j++) {
                            enemyBolts.create(message.bolts[i].coordinates[j].x, message.bolts[i].coordinates[j].y, 'bolt');
                         }
                    }
                }
            } else if(message.type == "death_message") {
                console.log(message.id + "dead");
                if (message.id == player.id) {
                    player.playerDead();
                }
            } else if(message.type == "resurrect_player") {
                console.log(message.id + "resurrected");

                if (message.id == player.id) {
                    player.playerAlive();
                }
            }
        };
    }

    send(message) {
        if (webSocket.socket.readyState == 1) {
            webSocket.socket.send(message);
        }
    }
}