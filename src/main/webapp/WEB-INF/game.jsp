<!doctype html> 
<html lang="en"> 
<head> 
	<meta charset="UTF-8" />

    <title>Phaser</title>
	<script type="text/javascript" src="assets/js/libs/phaser.min.js"></script>
    <script type="text/javascript" src="assets/js/libs/jquery.min.js"></script>


	<script type="text/javascript" src="assets/js/player.js"></script>
	<script type="text/javascript" src="assets/js/socket.js"></script>

    <!--<link rel="stylesheet" type="text/css" href="assets/mincss/style.min.css" >
       -->
    <script type="text/javascript" src="assets/js/game.js"></script>
</head>
<body>


	<div id="score">Score: 0 </div>

	<div id="play_ground"> </div>
	<script>
		start("${serverHostName}", "${serverPort}");
	</script>
</body>
</html>