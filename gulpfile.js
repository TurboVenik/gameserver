var gulp = require('gulp'),
	cssmin = require('gulp-cssmin'),
	rename = require('gulp-rename');
 
gulp.task('default', function () {
    gulp.src('src/main/webapp/WEB-INF/assets/css/*.css')
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('src/main/webapp/WEB-INF/assets/mincss'));

    gulp.src('bower_components/jquery/dist/jquery.min.js')
        .pipe(gulp.dest('src/main/webapp/WEB-INF/assets/js/libs'));

    gulp.src('bower_components/phaser/build/phaser.min.js')
        .pipe(gulp.dest('src/main/webapp/WEB-INF/assets/js/libs'));
});